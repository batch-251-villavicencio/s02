leapyear = input("Please input a year\n")
if leapyear.lstrip('-').isdigit():
    if int(leapyear) > 0:
        if int(leapyear)%4 == 0:
            print(f"{leapyear} is a leap year")
        else:
            print(f"{leapyear} is not a leap year")
    else:
        print("Error! Please use positive numbers")
else:
    print("Error! Wrong datatype input")
row = int(input("Enter a number of rows\n"))
col = int(input("Enter a number of columns\n"))

i = 0
j = 0
asterisk = ""
while i<row:
    while j<col:
        asterisk = asterisk + "*"
        j+=1
    print(asterisk)
    i+=1

